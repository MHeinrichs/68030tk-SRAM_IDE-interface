# 68030-SRAM_IDE
 
This is the 4MB SRAM-expansion for the 68030 [CPU turbo-card](https://gitlab.com/MHeinrichs/68030tk). It was originally designed for the [Harms Professional 530-Board](http://amiga.resource.cx/exp/professional530) and still runs with it with a special firmware.

![Ram-Card with DOM](https://gitlab.com/MHeinrichs/68030tk-SRAM_IDE-interface/raw/master/Pics/01.Board.JPG)

The RAM is autoconfig Zorro II mem. The IDE works from Kickstart 1.3 to 3.1.4 !

The SRAM runs 68030 buscycles with one waitstates via STERM. However SOME 55ns SRAMs are fast enugh to run 0WS-Cycles! The IDE can make up to 10MB/s and provides power via the "IDE-key pin" for some DOMs.

## Things to improve
The power supply is done via the VG 96 connector . This is not sufficient for some CF-Cards. These cards need power from somewhere else!

**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!

## Theory of operation
* This PCB has four layers and consists of the two SD-RAM & 32 bit bus width, a PLL-Clock to double the 50MHz clock, a Xilinx XC95144XL, a bootrom, some bus bufferes for the IDE port and latches to provide the 3.3V levelshift for the SD-RAMs. 
* The CPLD does three things:
    * It makes the Autoconfig for the 64MB in Zorro III
    * It accesses the SRAM at the propper adresses (very easy statemachine ;) )
    * It controlls the IDE

## What is in this repository?
This repository consists of several directories:
* PCB: The board, schematic and bill of material (BOM) are in this directory. 
* Logic: Here the code, project files and pin-assignments for the CPLD is stored. There are two versions: V3 is for the Harms turbocard and V4 for my 68030-Card
* Pics: Some pictures of this project
* root directory: the readme

## How to open the PCB-files?
You find the eagle board and schematic files (brd/sch) for the pcb. The software can be [downloaded](https://www.autodesk.com/products/eagle/overview) for free after registration. KiCad can import these files too. But importers are not 100% reliable... 

## How to make a PCB?
You can generate Gerber-files from the brd files according to the specs of your PCB-manufactor. However, you can try to use the Gerber-files provided. Some specs: min trace width: 0.15mm/6mil, min trace dist: 0.15mm/6mil, min drill: 0.3mm

**ATTENTION: THE PCB has 4 layers!**

## How to get the parts?
Most parts can be found on digikey or mouser or a parts supplier of your choice. I recoment Alliance AS6C8C16-55ZIN for SRAM. 
A list of parts is found in the file [Harms Professional RAM-16-4c.txt](https://gitlab.com/MHeinrichs/68030tk-SRAM_IDE-interface/raw/master/Schematic%20and%20PCBs/Harms%20Professional%20RAM-16-4c.txt)

## How to programm the board?
The CPLD must be programmed via Xilinx IMPACT and an apropriate JTAG-dongle. The JED-File is provided. 

## It doesn't work! How to I get help?
For support visit the [a1k-forum](https://www.a1k.org/forum/showthread.php?t=44245). Yes, you need to register and it's German, but writing in English is accepted. For bug-reports write a ticket here ;). 

